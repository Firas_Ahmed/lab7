#include <iostream>
using namespace std;
struct Employee{
    string First_Name, Last_Name, Email, Phone_No;
    int ID;
    float salary;
    Employee *manager;
};

int main() {
    int size; 
    std::cout << "Enter the number of staff: ";
    cin >> size;
    Employee staff[size];
    for(int i = 0; i < size; i++){
        string fn, ln, em, pn;
        float sal; Employee *man;

        cout << "\nPlease enter the employee details below";

        cout << "\nFirst name: ";
        cin >> fn;
        staff[i].First_Name = fn;

        cout << "\nLast name: ";
        cin >> ln;
        staff[i].Last_Name = ln;

        cout << "\nEmail: ";
        cin >> em;
        staff[i].Email = em;

        cout << "\nPhone number: ";
        cin >> pn;
        staff[i].Phone_No = pn;

        cout << "\nSalary: ";
        cin >> sal;
        staff[i].salary = sal;

        staff[i].ID = (i+1);
    }

    cout << "\n\nEmployees:-";

    for(int i = 0; i < size; i++){
        cout << "\nName: " << staff[i].First_Name << " " << staff[i].Last_Name;
        cout << "\tID: " << staff[i].ID;
    }
}
