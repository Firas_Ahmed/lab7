#include <iostream>
using namespace std;

void array_print(char arr[], int size){
    for(int i = 0; i < size; i++){
        if(i == (size-1)){
            cout << arr[i];
        }else{
            cout << arr[i];
        }
    }
}

void reverse(char arr[], int size){
    int end = size-1;
    for(int i = 0; i < end; i++){
        char temp = arr[i];
        arr[i] = arr[end];
        arr[end] = temp;
        end--;
    }
    cout << arr;
}

int main() {
    char name[] = "Harry";
    int arr_size = sizeof(name)/sizeof(name[0]);
    reverse(name, arr_size);
    array_print(name, arr_size);
} 
