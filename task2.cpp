#include <iostream>
using namespace std;
int main() {
  double num = 2.4;
  double *p = &num;
  cout << "variable value: " << num << " variable address: " << &num;
  cout << "\np value: " << p << " p address: " << &p << " dereference: " << *p;
}
