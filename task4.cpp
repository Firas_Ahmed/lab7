#include <iostream>
using namespace std;

struct Employee{
    string First_Name, Last_Name, Email, Phone_No;
    int ID;
    float salary;
    Employee *manager;
};

void print_emp(Employee& e) {
    cout << "Name: " << e.First_Name << " " << e.Last_Name;
    cout << ", ID: " << e.ID;
    cout << ", Email: " << e.Email;
    cout << ", Phone number: " << e.Phone_No;
    cout << ", Salary: $" << e.salary << "\n";

    if (e.manager != nullptr) {
    cout << "Manager Name: " << e.manager->First_Name << " " << e.manager->Last_Name << "\n";
    }
}

int main() {
    Employee John = {"John", "Doe", "JD@email.com", "0501111111", 1,50000, nullptr};
    Employee Bill = {"Bill", "Bill", "BB@email.com", "0502222222", 2,7000, &John};
    Employee Vin = {"Vincent", "Pizzapasta", "VP@email.com", "0503333333", 3,7000, &John};

    print_emp(John);
    print_emp(Bill);
    print_emp(Vin);
}
